(function($){
  // Get logged in user then update the page with the name and photo of the
  // logged in user
  AP.getUser(function(user){
    AP.request({
      url: "/rest/api/2/user?username=" + user.id,
      success: function(data){
        var user = JSON.parse(data);
        $('.ap3-user-display-name').html(user.displayName);
        $('.ap3-user-avatar').attr('src', user.avatarUrls['48x48']);
        AP.resize();
      }
    })
  });

  // Handle page navigation. AUI's default page navigation requires multiple pages
  $('.aui-nav>li>a').on('click',function(){
    var $this = $(this);
    $('.aui-nav>li').removeClass('aui-nav-selected');
    $this.parent().addClass('aui-nav-selected');
    $('section.aui-page-panel-content').addClass('hidden');
    $('#'+$this.data('id')).removeClass('hidden');
    return false;
  });

  // Resize iframe every 50ms. TODO find a better alternative
  setInterval(AP.resize,50)

})(AJS.$);