<section id="content" role="main">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-image">
                <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img class="ap3-user-avatar" src="">
                    </div>
                </div>
            </div>
            <div class="aui-page-header-main">
                <h1>{{greeting}} <span class="ap3-user-display-name"></span>!</h1>
                <p>You're looking at a page inside your new add-on. It's now ready for you to make it awesome!</p>
            </div>
            <div class="aui-page-header-actions">
                <div class="aui-buttons">
                    <a class="aui-button" href="https://remoteapps.jira.com/wiki/display/ARA/Interactive+Descriptor+Reference" target="_blank"><span class="aui-icon aui-icon-small aui-iconfont-info">Guide</span> Descriptor Guide</a>
                    <a class="aui-button" href="https://remoteapps.jira.com/wiki/display/ARA/Home" target="_blank"><span class="aui-icon aui-icon-small aui-iconfont-help">Help</span> Help</a>
                </div>
            </div>
        </div>
    </header>
</section>

<nav class="aui-navgroup aui-navgroup-horizontal">
    <div class="aui-navgroup-inner">
        <div class="aui-navgroup-primary">
            <ul class="aui-nav">
                <li class="aui-nav-selected"><a data-id="getting-started" href="#">Getting Started</a></li>
                <li><a data-id="snoop" href="#">Inspector</a></li>
            </ul>
        </div><!-- .aui-navgroup-primary -->
    </div><!-- .aui-navgroup-inner -->
</nav>

<div class="aui-page-panel">
    <div class="aui-page-panel-inner">
        <section id="getting-started" class="aui-page-panel-content">
            <div class="aui-group ap3-vid-container">
              <h1>Building add-ons has never been easier</h1>
              <div class="ap3-vid">
                <iframe width="800" height="450" src="https://www.youtube.com/embed/pzDPaQe24zo?feature=player_detailpage&hd=1" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
            <h2>Getting started with your new add-on</h2>
            <p>
              You're looking at your new add-on. When you ran <span class="aui-label"><code>ap3 start p3</code></span>
              in your terminal, the AP3 tool booted up a local plugin web server in development mode, exposed your your local web server
              to the internet (at <a href="{{localBaseUrl}}">{{localBaseUrl}}</a>), then registered your plugin into this Atlassian product.
            </p>
            <p>
              This add-on is built with JavaScript using the Ringo-Kit framework built into the Plugins 3
              platform. Ringo-Kit extends <a href="http://ringojs.org/">Ringo</a>, a CommonJS-based
              JavaScript runtime written in Java and based on the Mozilla Rhino JavaScript engine.
            </p>
            <p>
              To get started, do the following:
            </p>
            <ol>
              <li><a href="https://remoteapps.jira.com/wiki/display/ARA/Home">Read the Plugins 3 guides</a></li>
              <li>Checkout what plugin points exist using the <a href="https://remoteapps.jira.com/wiki/display/ARA/Interactive+Descriptor+Reference">interactive plugin module descriptor guide</a></li>
              <li>See to the <a href="https://developer.atlassian.com/static/ap3/ringo-kit/doc/">Ringo-Kit API reference</a> to see what you can do with Ringo-Kit (lots of work still to be done)</li>
              <li>Start coding!</li>
            </ol>
            <p>
              ...and BTW, don't forget to checkout what you can do with the new AP3 tools. Just <span class="aui-label"><code>ap3 help</code></span>
              to explore.
            </p>
        </section>
        <section id="snoop" class="aui-page-panel-content hidden">
            <div class="aui-tabs vertical-tabs" id="tabs-example">
                <ul class="tabs-menu">
                    <li class="menu-item active-tab">
                        <a href="#context-parameters"><strong>Context Parameters</strong></a>
                    </li>
                    <li class="menu-item">
                        <a href="#query-parameters"><strong>Query Parameters</strong></a>
                    </li>
                </ul>
                <div class="tabs-pane active-pane" id="context-parameters">
                    <p>
                      The following are properties available through the default context object
                      in your template. You can access these in your view using this format:
                      &#123;&#123;property&#125;&#125;.
                    </p>
                    <table class="aui">
                      <thead>
                        <tr>
                          <th>Property Name</th>
                          <th>Value</th>
                        </tr>
                      </thead>
                      <tbody>
                        {{#each ctx}}
                        <tr>
                          <td><span id="unclickable-label" class="aui-label">{{key}}</span></td>
                          <td>{{value}}</td>
                        {{/each}}
                      </tbody>
                    </table>
                </div>
                <div class="tabs-pane" id="query-parameters">
                    <p>
                        The following are the query parameters passed into the iframe:
                    </p>
                    <table class="aui">
                      <thead>
                        <tr>
                          <th>Property Name</th>
                          <th>Value</th>
                        </tr>
                      </thead>
                      <tbody>
                        {{#each query}}
                        <tr>
                          <td><span id="unclickable-label" class="aui-label">{{key}}</span></td>
                          <td>{{value}}</td>
                        {{/each}}
                      </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>